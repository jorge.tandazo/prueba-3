var express = require('express');
var router = express.Router();
var retencion = require('../controladores/controladorsw');
var retenciones = new retencion();
var rt = require('../controladores/controladorRetencion');
var controladorRetencion = new rt();
//importacion de controladores


/* GET home page. */



router.get('/', function (req, res, next) {
    var login = (req.session !== undefined && req.session.albert !== undefined);
    if (login) {
        res.render('plantilla', {
            title: 'Sistema de Retenciones',
            login: login,
            fragmento: 'fragmentos/fr_bienvenido'
        });
    } else {
        res.render('plantilla', {
            title: 'Sistema de Retenciones',
            login: login
        });
    }

});

router.get('/inicio_sesion', function (req, res, next) {
    res.render('login', {title: 'Inicio de sesion'});
});
router.get('/listarR',retenciones.obtenerLista);
router.get('/clientes', retenciones.obtenerClientes);
router.get('/porcentaje', retenciones.obtenerPorcentaje);
//router.get('/busca',controladorRetencion.listar);
router.post('/guardarR',controladorRetencion.guardar);
router.get('/buscar/:fecha',retenciones.buscarporFecha)
router.get('/reporteg', controladorRetencion.generarReporte);

module.exports = router;
