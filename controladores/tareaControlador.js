'use strict';
var mongoose = require('mongoose');
var Tarea = require('../modelos/tarea');

class tareaController {

    presentar(req, res) {
        var login = (req.session !== undefined && req.session.albert !== undefined);
        Tarea.find({}, (err, tareas) =>
        {
            if (err)
                throw err;
            else
                res.render('tareas', {
                    title: 'Tareas',
                    tarea: tareas,
                    login: login
                });
        });
    }

    guardar(req, res) {
        new Tarea
                ({
                    id: new mongoose.Types.ObjectId(),
                    titulo: req.body.titulo,
                    descripcion: req.body.descripcion,
                    estado: req.body.estado
                }).save(function (err, newTarea)
        {
            if (err)
            {
                console.log("Nose pudo guardar");
            } else if (newTarea)
            {
                console.log("Se ha guardado");
            }
            res.redirect('/tareas');
        });
    }

    modificar(req, res) {
        Tarea.update({id: req.body.oculto}, {$set: {titulo: req.body.titulo, descripcion: req.body.descripcion, estado: req.body.estado}}
        , (err, tarea) => {
            if (err)
            {
                console.log("no se pudo modificar");
                //req.flash('info', 'no se pudo modificar');
            } else if (tarea)
            {
                console.log("Se modifico correctamente");
                //req.flash('info', 'Se modifico correctamente');
            }
            res.redirect('/tareas');
        });
    }

    buscar(req, res) {
        const login = true//utilidades.verLogin(req);
        if (req.body.campo_buscar === "tarea") {
            Tarea.find({titulo: {$regex: ".*" + req.body.nombre_buscar, $options: "i"}}, (err, busqueda) => {
                if (busqueda) {
                    res.render('tareas', {
                        login: login,
                        tarea: busqueda
                    });
                } else if (err) {
                    //req.flash('info', 'Hubo un error');
                    res.redirect('/tareas');
                }
            });
        } else if (req.body.campo_buscar === "descripcion"){
            Tarea.find({descripcion: {$regex: ".*" + req.body.nombre_buscar, $options: "i"}}, (err, busqueda) => {
                if (busqueda) {
                    res.render('tareas', {
                        login: login,
                        tarea: busqueda
                    });
                } else if (err) {
                    //req.flash('info', 'Hubo un error');
                    res.redirect('/tareas');
                }
            });
            
        }
    }

}

module.exports = tareaController;




