var Cuenta = require('../modelos/cuenta');

class cuentaControlador {
    iniciar_sesion(req, res) {
        Cuenta.findOne({"correo": req.body.correo}, (err, cuenta) => {
            if (err) {
                console.log("Su cuenta no existe");
                req.flash('info', 'Su cuenta no existe');
            } else if (cuenta) {
                if (cuenta.clave === req.body.clave) {
                    req.session.albert = cuenta.external_id;
                    console.log("La sesion esta iniciada");
                    res.redirect('/');
                } else {
                    console.log("Sus credenciales no son las correctas");
                    req.flash('info', 'Sus credenciales no son las correctas');
                }
            } else {
                console.log("Su cuenta no existe");
                req.flash('info', 'Su cuenta no existe');
            }
        });
    }

    cerrar_sesion(req, res) {
        req.session.destroy();
        res.redirect('/');
    }
}

module.exports = cuentaControlador;






