'use strict';
const uuidv4 = require('uuid/v4');
var mongoose = require('mongoose');
var Persona = require('../modelos/persona');
var Cuenta = require('../modelos/cuenta');

class personaControlador {
    listar(req, res) {
        Persona.find({}, (err, personaListar) =>
        {
            if (err) {
                console.log("Hubo un error " + err);
                res.send("Hubo un error " + err);
            } else {
                console.log("OK " + personaListar);
                res.send(personaListar);
            }
        }
        );
    }

    guardar(req, res) {
        Persona.findOne({'cedula': req.body.cedula}, (err, persona) => {
            if (err) {
                res.redirect('/registro');

            } else if (persona) {
                console.log(persona);
                req.flash('info', 'Sus cuenta ya existe');
            } else {
                new Persona({
                    id: new mongoose.Types.ObjectId(),
                    external_id: uuidv4(),
                    cedula: req.body.cedula,
                    apellidos: req.body.apellidos,
                    nombres: req.body.nombres,
                    direccion: req.body.direccion,
                    telefono: req.body.telefono
                }).save(function (err, newPersona) {
                    if (err) {
                        res.redirect('/registro');
                    } else if (newPersona) {
                        new Cuenta({
                            id: new mongoose.Types.ObjectId(),
                            external_id: uuidv4(),
                            correo: req.body.correo,
                            clave: req.body.clave,
                            persona: newPersona.id
                        }).save(function (err, newCuenta) {
                            if (err) {
                                res.redirect('/registro');
                            } else {
                                res.redirect('/inicio_sesion');
                            }
                        });
                    }
                });
            }
        });
    }

}

module.exports = personaControlador;



