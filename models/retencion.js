module.exports = function (sequelize, Sequelize) {
    var Retencion = sequelize.define('retencion', {
        id: {
            autoIncrement: true,
            primaryKey: true,
            type: Sequelize.INTEGER
        },
        external_id: {
            type: Sequelize.UUID,
           defaultValue: Sequelize.UUIDV1
        },
        cliente: {
            type: Sequelize.STRING(50)
        },

        nrofactura: {
            type: Sequelize.STRING(50)
        },

        monto: {
            type: Sequelize.DOUBLE(10,2)
        },
        porcentaje_clas: {
            type: Sequelize.DOUBLE(10,2)
        },

        fechaRetencion: {
            type: Sequelize.DATEONLY
        },

        valorRetencion: {
            type: Sequelize.DOUBLE(10,2)
        }
    }, {freezeTableName: true,
        createdAt: 'fecha_registro',
        updatedAt: 'fecha_modificacion'
        });


    return Retencion;
};
